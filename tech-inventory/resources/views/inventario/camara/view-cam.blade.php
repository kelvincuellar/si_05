<div class="col-md-12">
    <div class="card">

      <div class="card-body">
        <div class="toolbar">
          <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="material-datatables">
          <table id="datatables10" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Serial</th>
                  <th>Resolución  de grabacion</th>
                  <th>Micrófono</th>
                  <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Resolución  de grabacion</th>
                    <th>Micrófono</th>
                    <th>Acciones</th>
                </tr>
              </tfoot>
            <tbody>
                @foreach($camaras as $camara)
                  <tr>
                  <td>{{ $camara->marca }}</td>
                  <td>{{ $camara->modelo }}</td>
                  <td>{{ $camara->serial }}</td>
                  <td>{{ $camara->resolucion_grabacion }}</td>
                  <td>{{ $camara->microfono }}</td>
                  <td>
                        <form id="camaraFormUrl" action="" method="POST">
                                @csrf @method('DELETE')
                            </form>
                        <a href="{{ route('edit.camara', encrypt($camara->id)) }}" class="btn btn-link btn-warning btn-just-icon edit" ><i class="material-icons">dvr</i></a>
                             <button class="btn btn-link btn-danger btn-just-icon remove" onclick="deleteCamara({{ $camara->id }})"><i class="material-icons">close</i></button>

                  </td>
                  </tr>

                @endforeach

            </tbody>
          </table>
        </div>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
<!-- end row -->

