<div class="col-md-12">
    <div class="card ">
      <div class="card-header ">
        <h4 class="card-title">Camaras |
          <small class="description">Aquí puedes administrar tus camaras</small>
        </h4>
      </div>
      <div class="card-body ">
        <ul class="nav nav-pills nav-pills-danger" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#camaraView" role="tablist">
              Registradas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " data-toggle="tab" href="#camaraFrm" role="tablist">
              Nuevo registro
            </a>
          </li>
        </ul>
        <div class="tab-content tab-space">
          <div class="tab-pane active" id="camaraView">
          @include('inventario.camara.view-cam')
          </div>
          <div class="tab-pane " id="camaraFrm">
                @include('inventario.camara.form-cam')
          </div>

        </div>
      </div>
    </div>
  </div>
