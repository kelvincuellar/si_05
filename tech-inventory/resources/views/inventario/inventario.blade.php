
@extends('layouts.principal-template',['activePage' => 'inventario','pageTitle' => 'Inventario'])
@section('content')



      <div class="row">
        <div class="col-md-12 ml-auto mr-auto">
          <div class="page-categories">
            <h3 class="title text-center">Seleccionar el tipo de dispositivo que deseas ingresar</h3>
            <br />
            <ul class="nav nav-pills nav-pills-warning nav-pills-icons justify-content-center" role="tablist">
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#pcTab" role="tablist">
                  <i class="material-icons">computer</i> Pc
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#monitorTab" role="tablist">
                  <i class="material-icons">tv</i> Monitor
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#cpuTab" role="tablist">
                  <i class="material-icons">memory</i> Cpu
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#mouseTab" role="tablist">
                  <i class="material-icons">mouse</i> Mouse
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tecladoTab" role="tablist">
                  <i class="material-icons">keyboard</i> Teclado
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#upsTab" role="tablist">
                  <i class="material-icons">battery_charging_full</i> Ups
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#camTab" role="tablist">
                  <i class="material-icons">videocam</i> camara
                </a>
              </li>

            </ul>
            <div class="tab-content tab-space tab-subcategories">
              <div class="tab-pane" id="pcTab">
                @include('inventario.pc.index-pc')
              </div>
              <div class="tab-pane active" id="monitorTab">
               @include('inventario.monitor.index')
              </div>
              <div class="tab-pane" id="cpuTab">
                @include('inventario.cpu.index')
              </div>
              <div class="tab-pane" id="mouseTab">
                @include('inventario.mouse.index')
              </div>
              <div class="tab-pane" id="tecladoTab">
                @include('inventario.teclado.index')
              </div>
              <div class="tab-pane" id="camTab">
                    @include('inventario.camara.index-cam')
                  </div>
              <div class="tab-pane" id="upsTab">
                <div class="card">
                    @include('inventario.ups.index')
              </div>

            </div>
          </div>
        </div>
      </div>

      <script>
            function setFormValidation(id) {
              $(id).validate({
                highlight: function(element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                  $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
                },
                success: function(element) {
                  $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                  $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
                },
                errorPlacement: function(error, element) {
                  $(element).closest('.form-group').append(error);
                },
              });
            }

            $(document).ready(function() {
              setFormValidation('#formularioUps');
              setFormValidation('#formularioCTeclado');
              setFormValidation('#pcFrm');
              setFormValidation('#formularioMouse');
              setFormValidation('#formularioMonitor');
              setFormValidation('#formularioCpu');
              setFormValidation('#formularioCamara');

            });
          </script>

@endsection


