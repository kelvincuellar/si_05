<div class="col-md-12 ml-auto mr-auto">

        <div class="card ">

          <div class="card-body ">
                  <div class="row col-md-9 ml-3 mt-4">
                  <p class="text-muted"><h4>LLena el siguiente formulario:</h4></span></p>

                        </div>
                        <hr>
                        {{ Form::open(['route' => 'store.mouse','method' => 'post' ,'id' => 'formularioMouse']) }}
            <div class="row my-2">
              <label class="col-sm-2 col-form-label">Marca <span class="text-danger">*</span></label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                  <input class="form-control" type="text" name="marca" id="marca" required="true"  />
                </div>
              </div>
                  <label class="col-sm-2 col-form-label">Modelo <span class="text-danger">*</span></label>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                    <input class="form-control" type="text" name="modelo" id="modelo"  required="true" />
                    </div>
                  </div>

                </div>

            <div class="row my-2">
              <label class="col-sm-2 col-form-label">Serial<span class="text-danger"> *</span></label>
              <div class="col-12 col-md-4">
                 <div class="form-group">
                    <input class="form-control" type="text" name="serial" id="serial"  required="true" />
                  </div>
              </div>


              <label class="col-sm-2 col-form-label">Código<span class="text-danger"> *</span></label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                  <input class="form-control" type="text" name="codigo" id="codigo" text="true" required="true" />
                </div>
              </div>

            </div>
            <div class="row my-2">
                  <label class="col-sm-2 col-form-label">Botones <span class="text-danger">*</span></label>
                  <div class="col-12 col-md-4">
                        <div class="form-group">
                                <input class="form-control" type="text" name="botones" id="botones" text="true" required="true" />
                              </div>
                  </div>

                      <label class="col-sm-2 col-form-label">Dpi <span class="text-danger">*</span></label>
                      <div class="col-12 col-md-4">
                        <div class="form-group">
                          <input class="form-control" type="text" name="dpi" id="dpi" />
                          <!--<input class="form-control" type="number" name="nombre" id="nombre" required="true" />-->
                        </div>
                      </div>

                    </div>
                    <div class="row my-2">
                          <label class="col-sm-2 col-form-label">Sensor <span class="text-danger">*</span></label>
                          <div class="col-12 col-md-4">
                            <div class="form-group">
                              <input class="form-control" type="text" name="sensor" id="sensor" number="true"  />
                            </div>
                          </div>


                              <label class="col-sm-2 col-form-label">Mano <span class="text-danger">*</span></label>
                              <div class="col-12 col-md-4">
                                <div class="form-group">
                                        {{ Form::select('mano',['Mano' => $mano], old('mano'),['placeholder' => 'Selecciona una opción', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '5' ,'id' => 'id_tipo_mano', 'name' => 'id_tipo_mano', 'required' => 'true']) }}
                                </div>
                              </div>

                            </div>


          </div>
          <div class="card-footer ml-auto mr-auto">
            <button  id="submiToma" class="btn btn-rose">Registrar</button>
          </div>
        </div>
        {{ Form::close() }}
    </div>


