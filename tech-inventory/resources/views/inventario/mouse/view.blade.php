<div class="col-md-12">
    <div class="card">

      <div class="card-body">
        <div class="toolbar">
          <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="material-datatables">
          <table id="datatables5" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Serial</th>
                  <th>Botones</th>
                  <th>Dpi</th>
                  <th>Mano</th>
                  <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Botones</th>
                    <th>Dpi</th>
                    <th>Mano</th>
                    <th>Acciones</th>
                </tr>
              </tfoot>
            <tbody>
                @foreach($mouse as $mice)
                  <tr>
                  <td>{{ $mice->marca }}</td>
                  <td>{{ $mice->modelo }}</td>
                  <td>{{ $mice->serial }}</td>
                  <td>{{ $mice->botones }}</td>
                  <td>{{ $mice->dpi }}</td>
                  <td>{{ $mice->id_tipo_mano }}</td>
                  <td>
                        <form id="mouseFormUrl" action="" method="POST">
                                @csrf @method('DELETE')
                            </form>
                        <a href="{{ route('edit.mouse', encrypt($mice->id)) }}" class="btn btn-link btn-warning btn-just-icon edit" ><i class="material-icons">dvr</i></a>
                             <button class="btn btn-link btn-danger btn-just-icon remove" onclick="deleteMouse({{ $mice->id }})"><i class="material-icons">close</i></button>
                  </td>
                  </tr>

                @endforeach

            </tbody>
          </table>
        </div>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
<!-- end row -->
