<div class="col-md-12 ml-auto mr-auto">

      <div class="card ">

        <div class="card-body ">
                <div class="row col-md-9 ml-3 mt-4">
                <p class="text-muted"><h4>LLena el siguiente formulario:</h4></span></p>

                      </div>
                      <hr>
                      {{ Form::open(['route' => 'store.monitor','method' => 'post' ,'id' => 'formularioMonitor']) }}
          <div class="row my-2">
            <label class="col-sm-2 col-form-label">Marca <span class="text-danger">*</span></label>
            <div class="col-12 col-md-4">
              <div class="form-group">
                <input class="form-control" type="text" name="marca" id="marca" required="true"  />
              </div>
            </div>
                <label class="col-sm-2 col-form-label">Modelo <span class="text-danger">*</span></label>
                <div class="col-12 col-md-4">
                  <div class="form-group">
                  <input class="form-control" type="text" name="modelo" id="modelo"  required="true" />
                  </div>
                </div>

              </div>

          <div class="row my-2">
            <label class="col-sm-2 col-form-label">Serial<span class="text-danger"> *</span></label>
            <div class="col-12 col-md-4">
               <div class="form-group">
                  <input class="form-control" type="text" name="serial" id="serial"  required="true" />
                </div>
            </div>


            <label class="col-sm-2 col-form-label">Codigo<span class="text-danger"> *</span></label>
            <div class="col-12 col-md-4">
              <div class="form-group">
                <input class="form-control" type="text" name="codigo" id="codigo" text="true" required="true" />
              </div>
            </div>

          </div>
          <div class="row my-2">
                <label class="col-sm-2 col-form-label">Tipo de panel <span class="text-danger">*</span></label>
                <div class="col-12 col-md-4">
                  <div class="form-group">
                  {{ Form::select('panel',['Panel' => $panel], old('panel'),['placeholder' => 'Selecciona una opción', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '5' ,'id' => 'id_tipo_panel', 'name' => 'id_tipo_panel', 'required' => 'true']) }}
                  </div>
                </div>

                    <label class="col-sm-2 col-form-label">Tamaño <span class="text-danger">*</span></label>
                    <div class="col-12 col-md-4">
                      <div class="form-group">
                        <input class="form-control" type="text" name="tamaño" id="tamaño" />
                        <!--<input class="form-control" type="number" name="nombre" id="nombre" required="true" />-->
                      </div>
                    </div>

                  </div>
                  <div class="row my-2">
                        <label class="col-sm-2 col-form-label">Resolución <span class="text-danger">*</span></label>
                        <div class="col-12 col-md-4">
                          <div class="form-group">
                            <input class="form-control" type="text" name="resolucion" id="resolucion" number="true"  />
                          </div>
                        </div>


                            <label class="col-sm-2 col-form-label">Color <span class="text-danger">*</span></label>
                            <div class="col-12 col-md-4">
                              <div class="form-group">
                                <input class="form-control" type="text" name="color" id="color" number="true"/>
                              </div>
                            </div>

                          </div>
                          <div class="row my-2">
                                <label class="col-sm-2 col-form-label">Tiempo de respuesta<span class="text-danger">*</span></label>
                                <div class="col-12 col-md-4">
                                        <div class="form-group">
                                                <input class="form-control" type="text" name="tiempo_respuesta" id="tiempo_respuesta" number="true"/>
                                              </div>
                                </div>
                                <label class="col-sm-2 col-form-label">Tipo de monitor <span class="text-danger">*</span></label>
                                <div class="col-12 col-md-4">
                                  <div class="form-group">
                                  {{ Form::select('panel',['Panel' => $tipo], old('panel'),['placeholder' => 'Selecciona una opción', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '5' ,'id' => 'id_tipo_monitor', 'name' => 'id_tipo_monitor', 'required' => 'true']) }}
                                  </div>
                                </div>

                                  </div>


        </div>
        <div class="card-footer ml-auto mr-auto">
          <button  id="submiToma" class="btn btn-rose">Registrar</button>
        </div>
      </div>
      {{ Form::close() }}
  </div>

