<div class="col-md-12 ml-auto mr-auto">

    <div class="card ">

      <div class="card-body ">
              <div class="row col-md-9 ml-3 mt-4">
              <p class="text-muted"><h4>LLena el siguiente formulario:</h4></span></p>

                    </div>
                    <hr>
                    {{ Form::open(['route' => 'store.pc','method' => 'post' ,'id' => 'pcFrm']) }}
        <div class="row my-2">
          <label class="col-sm-2 col-form-label">Codigo de Barras </label>
          <div class="col-12 col-md-4">
            <div class="form-group">
              <input class="form-control" type="text" name="codigo_barra" id="codigo_barra" number="true"  />
            </div>
          </div>
              <label class="col-sm-2 col-form-label">Codigo Interno </label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                <input class="form-control" type="text" name="codigo_interno" id="codigo_interno"  required="true" placeholder="codigo sugerido:" />
                </div>
              </div>

            </div>

        <div class="row my-2">
          <label class="col-sm-2 col-form-label">Unidad de medida <span class="text-danger"> *</span></label>
          <div class="col-12 col-md-4">
            <div class="form-group">
            {{ Form::select('medida',['Unidades de medida' => ''], old('medida'),['placeholder' => 'Selecciona una unidad', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '7' ,'id' => 'id_unidad_medida', 'name' => 'id_unidad_medida', 'required' => "true"]) }}
            </div>
          </div>


          <label class="col-sm-2 col-form-label">Nombre del artículo <span class="text-danger"> *</span></label>
          <div class="col-12 col-md-4">
            <div class="form-group">
              <input class="form-control" type="text" name="nombre" id="nombre" text="true" required="true" />
            </div>
          </div>

        </div>
        <div class="row my-2">
              <label class="col-sm-2 col-form-label">Marca</label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                {{ Form::select('marca',['Marca' => ''], old('marca'),['placeholder' => 'Selecciona una marca', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '7' ,'id' => 'marca', 'name' => 'marca']) }}
                </div>
              </div>

                  <label class="col-sm-2 col-form-label">Presentación</label>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                      <input class="form-control" type="text" name="presentacion" id="presentacion" />
                      <!--<input class="form-control" type="number" name="nombre" id="nombre" required="true" />-->
                    </div>
                  </div>

                </div>
                <div class="row my-2">
                      <label class="col-sm-2 col-form-label">Mínimo</label>
                      <div class="col-12 col-md-4">
                        <div class="form-group">
                          <input class="form-control" type="text" name="minimo" id="minimo" number="true"  />
                        </div>
                      </div>


                          <label class="col-sm-2 col-form-label">Máximo</label>
                          <div class="col-12 col-md-4">
                            <div class="form-group">
                              <input class="form-control" type="text" name="maximo" id="maximo" number="true"/>
                            </div>
                          </div>

                        </div>
                        <div class="row my-2">
                              <label class="col-sm-2 col-form-label">Grupo<span class="text-danger">*</span></label>
                              <div class="col-12 col-md-4">
                                <div class="form-group">
                                {{ Form::select('grupo',['Grupo' => ''], old('grupo'),['placeholder' => 'Selecciona un grupo', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '7' ,'id' => 'id_grupo', 'name' => 'id_grupo', 'required' => 'true']) }}
                                </div>
                              </div>


                                  <label class="col-sm-2 col-form-label">Proveedor</label>
                                  <div class="col-12 col-md-4">
                                    <div class="form-group">
                                    {{ Form::select('provider',['Grupo' => ''], old('provider'),['placeholder' => 'Selecciona un Proveedor', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '7' ,'id' => 'id_proveedor', 'name' => 'id_proveedor']) }}
                                    </div>
                                  </div>

                                </div>


      </div>
      <div class="card-footer ml-auto mr-auto">
        <button  id="submiToma" class="btn btn-rose">Registrar</button>
      </div>
    </div>
    {{ Form::close() }}
</div>

