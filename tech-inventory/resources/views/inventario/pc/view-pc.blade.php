<div class="col-md-12">
    <div class="card">

      <div class="card-body">
        <div class="toolbar">
          <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="material-datatables">
          <table id="datatables6" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>serial</th>
                  <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>serial</th>
                    <th>Acciones</th>
                </tr>
              </tfoot>
            <tbody>


            </tbody>
          </table>
        </div>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
<!-- end row -->
