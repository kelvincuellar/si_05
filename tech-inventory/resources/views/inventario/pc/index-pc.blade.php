<div class="col-md-12">
    <div class="card ">
      <div class="card-header ">
        <h4 class="card-title">Pcs |
          <small class="description">Aquí puedes administrar tus pcs.</small>
        </h4>
      </div>
      <div class="card-body ">
        <ul class="nav nav-pills nav-pills-danger" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#pcView" role="tablist">
              Ingreso
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#pcFromu" role="tablist">
              registrados
            </a>
          </li>
        </ul>
        <div class="tab-content tab-space">
          <div class="tab-pane active" id="pcView">
          @include('inventario.pc.view-pc')
          </div>
          <div class="tab-pane" id="pcFromu">
                @include('inventario.pc.form-pc')
          </div>

        </div>
      </div>
    </div>
  </div>
