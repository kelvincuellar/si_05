<div class="col-md-12 ml-auto mr-auto">

        <div class="card ">

          <div class="card-body ">
                  <div class="row col-md-9 ml-3 mt-4">
                  <p class="text-muted"><h4>LLena el siguiente formulario:</h4></span></p>

                        </div>
                        <hr>
                        {{ Form::open(['route' => 'store.teclado','method' => 'post' ,'id' => 'formularioCTeclado']) }}
            <div class="row my-2">
              <label class="col-sm-2 col-form-label">Marca <span class="text-danger">*</span></label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                  <input class="form-control" type="text" name="marca" id="marca" required="true"  />
                </div>
              </div>
                  <label class="col-sm-2 col-form-label">Modelo <span class="text-danger">*</span></label>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                    <input class="form-control" type="text" name="modelo" id="modelo"  required="true" />
                    </div>
                  </div>

                </div>

            <div class="row my-2">
              <label class="col-sm-2 col-form-label">Serial<span class="text-danger"> *</span></label>
              <div class="col-12 col-md-4">
                 <div class="form-group">
                    <input class="form-control" type="text" name="serial" id="serial"  required="true" />
                  </div>
              </div>


              <label class="col-sm-2 col-form-label">Código<span class="text-danger"> *</span></label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                  <input class="form-control" type="text" name="codigo" id="codigo" text="true" required="true" />
                </div>
              </div>

            </div>
            <div class="row my-2">
                  <label class="col-sm-2 col-form-label">Color <span class="text-danger">*</span></label>
                  <div class="col-12 col-md-4">
                        <div class="form-group">
                                <input class="form-control" type="text" name="color" id="color" text="true" required="true" />
                              </div>
                  </div>

                      <label class="col-sm-2 col-form-label">Tamaño <span class="text-danger">*</span></label>
                      <div class="col-12 col-md-4">
                        <div class="form-group">
                          <input class="form-control" type="text" name="tamaño" id="tamaño" />
                          <!--<input class="form-control" type="number" name="nombre" id="nombre" required="true" />-->
                        </div>
                      </div>

                    </div>
                    <div class="row my-2">
                          <label class="col-sm-2 col-form-label">Idioma <span class="text-danger">*</span></label>
                          <div class="col-12 col-md-4">
                            <div class="form-group">
                              <input class="form-control" type="text" name="idioma" id="idioma" number="true"  />
                            </div>
                          </div>


                              <label class="col-sm-2 col-form-label">Tipo <span class="text-danger">*</span></label>
                              <div class="col-12 col-md-4">
                                <div class="form-group">
                                        {{ Form::select('tipo_teclado',['Tipo Teclado' => $tipo_teclado], old('tipo_teclado'),['placeholder' => 'Selecciona una opción', 'class' => 'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '5' ,'id' => 'id_tipo_teclado', 'name' => 'id_tipo_teclado', 'required' => 'true']) }}
                                </div>
                              </div>

                            </div>

          </div>
          <div class="card-footer ml-auto mr-auto">
            <button  id="submiToma" class="btn btn-rose">Registrar</button>
          </div>
        </div>
        {{ Form::close() }}
    </div>


