<div class="col-md-12">
    <div class="card">

      <div class="card-body">
        <div class="toolbar">
          <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="material-datatables">
          <table id="datatables3" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Codigo</th>
                  <th>Tamaño</th>
                  <th>Tipo panel</th>
                  <th>Resolución</th>
                  <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Codigo</th>
                    <th>Tamaño</th>
                    <th>Tipo panel</th>
                    <th>Resolución</th>
                    <th>Acciones</th>
                </tr>
              </tfoot>
            <tbody>
                @foreach($teclados as $teclado)
                  <tr>
                  <td>{{ $teclado->marca }}</td>
                  <td>{{ $teclado->modelo }}</td>
                  <td>{{ $teclado->serial }}</td>
                  <td>{{ $teclado->tamaño }}</td>
                  <td>{{ $teclado->color }}</td>
                  <td>{{ $teclado->idioma }}</td>
                  <td>
                        <form id="tecladoFormUrl" action="" method="POST">
                                @csrf @method('DELETE')
                            </form>
                        <a href="{{ route('edit.teclado', encrypt($teclado->id)) }}" class="btn btn-link btn-warning btn-just-icon edit" ><i class="material-icons">dvr</i></a>
                             <button class="btn btn-link btn-danger btn-just-icon remove" onclick="deleteTeclado({{ $teclado->id }})"><i class="material-icons">close</i></button>
                  </td>
                  </tr>

                @endforeach

            </tbody>
          </table>
        </div>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
<!-- end row -->

