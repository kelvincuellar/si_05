<div class="col-md-12">
    <div class="card ">
      <div class="card-header ">
        <h4 class="card-title">Teclados |
          <small class="description">Aquí puedes administrar tus teclados.</small>
        </h4>
      </div>
      <div class="card-body ">
        <ul class="nav nav-pills nav-pills-danger" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tecladoView" role="tablist">
              Ingreso
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tecladoFrm" role="tablist">
              registrados
            </a>
          </li>
        </ul>
        <div class="tab-content tab-space">
          <div class="tab-pane active" id="tecladoView">
          @include('inventario.teclado.view')
          </div>
          <div class="tab-pane" id="tecladoFrm">
                @include('inventario.teclado.form')
          </div>

        </div>
      </div>
    </div>
  </div>
