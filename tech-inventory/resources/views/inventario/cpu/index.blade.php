<div class="col-md-12">
    <div class="card ">
      <div class="card-header ">
        <h4 class="card-title">Computadoras |
          <small class="description">Aquí puedes administrar tus computadoras</small>
        </h4>
      </div>
      <div class="card-body ">
        <ul class="nav nav-pills nav-pills-danger" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#cpuView" role="tablist">
              Registradas
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#cpuFrm" role="tablist">
              Nuevo registro
            </a>
          </li>
        </ul>
        <div class="tab-content tab-space">
          <div class="tab-pane active" id="cpuView">
          @include('inventario.cpu.view')
          </div>
          <div class="tab-pane" id="cpuFrm">
                @include('inventario.cpu.form')
          </div>

        </div>
      </div>
    </div>
  </div>
