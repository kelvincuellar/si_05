<div class="col-md-12">
    <div class="card">

      <div class="card-body">
        <div class="toolbar">
          <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="material-datatables">
          <table id="datatables4" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                  <th>Marca</th>
                  <th>Codigo</th>
                  <th>Ram</th>
                  <th>Disco</th>
                  <th>Motherboard</th>
                  <th>Procesador</th>
                  <th>Tipo</th>
                  <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                        <th>Marca</th>
                        <th>Codigo</th>
                        <th>Ram</th>
                        <th>Disco</th>
                        <th>Motherboard</th>
                        <th>Procesador</th>
                        <th>Tipo</th>
                    <th>Acciones</th>
                </tr>
              </tfoot>
            <tbody>
                @foreach($computadores as $cpu)
                  <tr>
                  <td>{{ $cpu->marca }}</td>
                  <td>{{ $cpu->serial }}</td>
                  <td>{{ $cpu->ram_pc }}</td>
                  <td>{{ $cpu->disco_pc }}</td>
                  <td>{{ $cpu->motherboard_pc }}</td>
                  <td>{{ $cpu->procesador_pc }}</td>
                  <td>{{ $cpu->id_tipo_computadora }}</td>
                  <td>
                        <form id="cpuFormUrl" action="" method="POST">
                                @csrf @method('DELETE')
                            </form>
                        <a href="{{ route('edit.cpu', encrypt($cpu->id)) }}" class="btn btn-link btn-warning btn-just-icon edit" ><i class="material-icons">dvr</i></a>
                             <button class="btn btn-link btn-danger btn-just-icon remove" onclick="deleteCpu({{ $cpu->id }})"><i class="material-icons">close</i></button>
                  </td>
                  </tr>

                @endforeach

            </tbody>
          </table>
        </div>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
<!-- end row -->

