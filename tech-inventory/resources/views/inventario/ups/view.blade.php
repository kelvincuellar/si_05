<div class="col-md-12">
    <div class="card">

      <div class="card-body">
        <div class="toolbar">
          <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="material-datatables">
          <table id="datatables7" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
            <thead>
              <tr>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Serial</th>
                  <th>Capacidad</th>
                  <th>Tipo</th>
                  <th>Acciones</th>
              </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>serial</th>
                    <th>capacidad</th>
                    <th>Tipo</th>
                    <th>Acciones</th>
                </tr>
              </tfoot>
            <tbody>
                @foreach($ups as $ups)
                  <tr>
                  <td>{{ $ups->marca }}</td>
                  <td>{{ $ups->modelo }}</td>
                  <td>{{ $ups->serial }}</td>
                  <td>{{ $ups->capacidad }}</td>
                  <td>{{ $ups->tipo_ups }}</td>
                  <td>
                        <form id="monitorFormUrl" action="" method="POST">
                                @csrf @method('DELETE')
                            </form>
                        <a href="{{ route('edit.ups', encrypt($ups->id)) }}" class="btn btn-link btn-warning btn-just-icon edit" ><i class="material-icons">dvr</i></a>
                             <button class="btn btn-link btn-danger btn-just-icon remove" onclick="deleteUps({{ $ups->id }})"><i class="material-icons">close</i></button>
                  </td>
                  </tr>

                @endforeach

            </tbody>
          </table>
        </div>
      </div>
      <!-- end content-->
    </div>
    <!--  end card  -->
  </div>
  <!-- end col-md-12 -->
<!-- end row -->

