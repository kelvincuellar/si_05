<div class="col-md-12">
    <div class="card ">
      <div class="card-header ">
        <h4 class="card-title">Ups |
          <small class="description">Aquí puedes administrar tus ups.</small>
        </h4>
      </div>
      <div class="card-body ">
        <ul class="nav nav-pills nav-pills-danger" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#upsView" role="tablist">
              Ingreso
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#upsFrm" role="tablist">
              registrados
            </a>
          </li>
        </ul>
        <div class="tab-content tab-space">
          <div class="tab-pane active" id="upsView">
          @include('inventario.ups.view')
          </div>
          <div class="tab-pane" id="upsFrm">
                @include('inventario.ups.form')
          </div>

        </div>
      </div>
    </div>
  </div>
