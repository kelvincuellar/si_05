<div class="col-md-12 ml-auto mr-auto">

        <div class="card ">

          <div class="card-body ">
                  <div class="row col-md-9 ml-3 mt-4">
                  <p class="text-muted"><h4>LLena el siguiente formulario:</h4></span></p>

                        </div>
                        <hr>
                        {{ Form::open(['route' => 'store.ups','method' => 'post' ,'id' => 'formularioUps']) }}
            <div class="row my-2">
              <label class="col-sm-2 col-form-label">Marca <span class="text-danger">*</span></label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                  <input class="form-control" type="text" name="marca" id="marca" required="true"  />
                </div>
              </div>
                  <label class="col-sm-2 col-form-label">Modelo <span class="text-danger">*</span></label>
                  <div class="col-12 col-md-4">
                    <div class="form-group">
                    <input class="form-control" type="text" name="modelo" id="modelo"  required="true" />
                    </div>
                  </div>

                </div>

            <div class="row my-2">
              <label class="col-sm-2 col-form-label">Serial<span class="text-danger"> *</span></label>
              <div class="col-12 col-md-4">
                 <div class="form-group">
                    <input class="form-control" type="text" name="serial" id="serial"  required="true" />
                  </div>
              </div>


              <label class="col-sm-2 col-form-label">Código<span class="text-danger"> *</span></label>
              <div class="col-12 col-md-4">
                <div class="form-group">
                  <input class="form-control" type="text" name="codigo" id="codigo" text="true" required="true" />
                </div>
              </div>

            </div>
            <div class="row my-2">
                  <label class="col-sm-2 col-form-label">Capacidad <span class="text-danger">*</span></label>
                  <div class="col-12 col-md-4">
                        <div class="form-group">
                                <input class="form-control" type="text" name="capacidad" id="capacidad" text="true" required="true" />
                              </div>
                  </div>

                      <label class="col-sm-2 col-form-label">Tipo de ups <span class="text-danger">*</span></label>
                      <div class="col-12 col-md-4">
                        <div class="form-group">
                          <input class="form-control" type="text" name="tipo_ups" id="tipo_ups" />
                          <!--<input class="form-control" type="number" name="nombre" id="nombre" required="true" />-->
                        </div>
                      </div>

                    </div>


          </div>
          <div class="card-footer ml-auto mr-auto">
            <button  id="submiToma" class="btn btn-rose">Registrar</button>
          </div>
        </div>
        {{ Form::close() }}
    </div>

