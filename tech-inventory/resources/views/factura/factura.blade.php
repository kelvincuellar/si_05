@extends('layouts.principal-template',['activePage' => 'factura','pageTitle' => 'Factura'])
@section('content')


      <div class="row">

<div class="col-md-8 ml-auto mr-auto">
    {{ Form::open(['url' => '/bodegas/store', 'method' => 'post']) }}
    {{ Form::token() }}
      <div class="card ">
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title"><i class="fas fa-warehouse"></i>Nuevo ingreso de equipo</h4>
          </div>
        </div>
        <div class="card-body ">
                <div class="row col-md-6 ml-auto mr-auto">
                    <center>
                <p class="text-muted"><h4> Centro de costos |</h4><span> <h2  class="font-weight-bold" style="color: #003854">{{ strtoupper('Ofinica Central') }}</h2></span></p>

                      </div>
                      <hr>

          <div class="row">
            <label class="col-sm-2 col-form-label">Nombre <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <div class="form-group">
                <input class="form-control" type="text" name="name" id="name" required="true" />
                <input type="hidden" name="id_costcenter" id="cost_center_id" value=""/>
              </div>
            </div>

          </div>

          <div class="row">
            <label class="col-sm-2 col-form-label">Comentario</label>
            <div class="col-sm-9">
              <div class="form-group">
                <input class="form-control" type="text" name="comentario" id="comentario" />
              </div>
            </div>

          </div>
          <div class="row">
            <label class="col-sm-2 col-form-label">Dirección</label>
            <div class="col-sm-9">
              <div class="form-group">
                <input class="form-control" type="text" name="address" id="direccion"/>
              </div>
            </div>

          </div>

        </div>
        <div class="card-footer ml-auto mr-auto">
          <button  id="submitBodega" class="btn btn-rose">Registrar</button>
        </div>
      </div>
      {{ Form::close() }}
  </div>
      </div>


@endsection
