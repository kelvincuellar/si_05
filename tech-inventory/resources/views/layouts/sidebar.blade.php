<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">

        TECH-INVENTORY
      </a>
    </div>
    <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item {{ ($activePage == 'dashbard') ? 'active': '' }}  ">
          <a class="nav-link" href="/">
            <i class="material-icons">person</i>
            <p>Métricas</p>
          </a>
        </li>
        <li class="nav-item {{ ($activePage == 'inventario') ? 'active': '' }} ">
        <a class="nav-link" href="{{ route('inventario') }}">
            <i class="material-icons">dashboard</i>
            <p>Inventario</p>
          </a>
        </li>
        <li class="nav-item {{ ($activePage == 'reportes') ? 'active': '' }} ">
          <a class="nav-link" href="./tables.html">
            <i class="material-icons">content_paste</i>
            <p>Reportes</p>
          </a>
        </li>

        <li class="nav-item {{ ($activePage == 'configuraciones') ? 'active': '' }}">
          <a class="nav-link" href="configuraciones">
            <i class="material-icons">bubble_chart</i>
            <p>Configuraciones</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./map.html">
            <i class="material-icons">location_ons</i>
            <p>Maps</p>
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="./notifications.html">
            <i class="material-icons">notifications</i>
            <p>Notifications</p>
          </a>
        </li>

      </ul>
    </div>
  </div>
