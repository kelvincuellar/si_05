!-- Button trigger modal -->


      <!-- Modal -->
      <div class="modal fade" id="storePerfil" tabindex="-1" role="dialog" aria-labelledby="storePerfilLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registrar un nuevo departamento</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                   {{ Form::open(['route' => 'store.perfil','method' => 'post','id' => 'StoreAreaForm']) }}
                             <div class="form-row">
                                    <div class="col-12">
                                      <input type="text" class="form-control" placeholder="cargo" name="cargo" id="cargo" required="true">
                                    </div>
                                    <div class="col-12 mt-5">
                                         <input type="text" class="form-control" placeholder="Digite sus dos nombres" name="nombres" id="nombres" required="true">
                                        </div>
                                        <div class="col-12 mt-5">
                                             <input type="text" class="form-control" placeholder="Digite sus dos apellidos" name="apellidos" id="apellidos" required="true">
                                            </div>

                                  </div>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">Registrar</button>
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
