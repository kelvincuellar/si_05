<!-- Button trigger modal -->


      <!-- Modal -->
      <div class="modal fade" id="storeDepartamento" tabindex="-1" role="dialog" aria-labelledby="storeDepartamentoLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registrar un nuevo departamento</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                   {{ Form::open(['route' => 'store.departamento','method' => 'post','id' => 'StoreDepartamentoForm']) }}
                            <div class="form-row">
                              <div class="col">
                                <input type="text" class="form-control" placeholder="Nombre del departamento" name="departamento" id="departamento" required="true">
                              </div>

                            </div>

            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">Registrar</button>
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
