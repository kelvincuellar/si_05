@extends('layouts.principal-template',['activePage' => 'configuraciones', 'pageTitle' => 'Configuraciones'])
@section('content')


        <div class="row ml-5">
               <h3><i class="material-icons text-danger">build</i> {{ strtoupper('Configuraciones') }}</h3>

        </div>
        <hr>
      <div class="row">

<div class="card mr-2 ml-5" style="width: 20rem;">
<img class="card-img-top" src="{{ asset('assets/img/departamento.jpg') }}" alt="Card image cap">
    <div class="card-body">
        <h4 class="font-weight-bold">Departamento</h4>
      <p class="card-text">En esta sección puedes administrar todos los departamentos.</p>
      <a href="{{ route('configuraciones.departamentos') }}" class="btn btn-primary col-12"><i class="material-icons">touch_app</i> Ingresar</a>
    </div>
  </div>
  <div class="card mx-2" style="width: 20rem;">
  <img class="card-img-top" src="{{ asset('assets/img/areas.jpg') }}" alt="Card image cap">
    <div class="card-body">
            <h4 class="font-weight-bold">Áreas</h4>
      <p class="card-text">En esta sección puedes administrar todas las áreas.</p>
    <a href="{{ route('configuraciones.areas') }}" class="btn btn-primary col-12"><i class="material-icons">touch_app</i> Ingresar</a>
    </div>
  </div>
  <div class="card mx-2" style="width: 20rem;">
  <img class="card-img-top" src="{{ asset('assets/img/perfiles.jpg') }}" alt="Card image cap">
    <div class="card-body">
            <h4 class="font-weight-bold">Perfiles</h4>
      <p class="card-text">En esta sección puedes administrar todos los perfiles.</p>
      <a href="{{ route('configuraciones.perfiles') }}" class="btn btn-primary col-12"><i class="material-icons">touch_app</i> Ingresar</a>
    </div>
  </div>

      </div>


  @endsection
