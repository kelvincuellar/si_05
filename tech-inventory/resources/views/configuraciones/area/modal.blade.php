


      <!-- Modal -->
      <div class="modal fade" id="storeArea" tabindex="-1" role="dialog" aria-labelledby="storeAreaLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Registrar un nuevo departamento</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    {{ Form::open(['route' => 'store.area','method' => 'post','id' => 'StoreAreaForm']) }}

                             <div class="form-row">
                               <div class="col-12">
                                 <input type="text" class="form-control" placeholder="Nombre del area" name="area" id="area" required="true">
                               </div>
                               <div class="col-12 mt-5">
                                     {{ Form::select('departametno',['Departamentos' => $departamento], old('departamento'),['placeholder' => 'Selecciona un departamento','class' =>'selectpicker col-12','data-style' => 'select-with-transition', 'data-size' => '7', 'name' => 'id_departamento', 'id' => 'id_departamento','required' => 'true']) }}
                                   </div>

                             </div>

             </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-danger">Registrar</button>
              {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
