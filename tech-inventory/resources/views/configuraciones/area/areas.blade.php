@extends('layouts.principal-template',['activePage' => 'configuraciones', 'pageTitle' => 'Áreas'])
@section('content')

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary card-header-icon">
            <div class="card-icon">
              <i class="material-icons">assignment</i>
            </div>
            <h4 class="card-title">Áreas</h4>
            <button type="button" class="btn btn-danger" style="float:right" data-toggle="modal" data-target="#storeArea"><i class="fas fa-plus-circle"></i> Agregar Area</button>
          </div>
          <div class="card-body">
            <div class="toolbar">
              <hr>
            </div>
            <div class="material-datatables">
              <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                <thead>
                  <tr>
                    <th>Área</th>
                    <th>Departamento</th>
                    <th class="disabled-sorting text-right">Actions</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Área</th>
                    <th>Departamento</th>
                    <th class="text-right">Actions</th>
                  </tr>
                </tfoot>
                <tbody>

                    @foreach($areas as $area)
                  <tr>
                  <td>{{ $area->area }}</td>
                  <td>{{ $area->departamento }}</td>
                    <td class="text-right">
                            {{ Form::open(['route' => ['delete.area', $area->id], 'method' => 'post', 'id' => 'ArDeleteForm']) }}
                            @method('DELETE')
                        {{ Form::close() }}
                      <a href="#" class="btn btn-link btn-warning btn-just-icon edit"><i class="material-icons">dvr</i></a>
                      <button class="btn btn-link btn-danger btn-just-icon remove" onclick="document.getElementById('ArDeleteForm').submit()"><i class="material-icons">close</i></button>
                    </td>
                  </tr>
                  @endforeach

                </tbody>
              </table>
            </div>
          </div>
          <!-- end content-->
        </div>
        <!--  end card  -->
      </div>
      <!-- end col-md-12 -->
    </div>
    <!-- end row -->

@include('configuraciones.area.modal')
@endsection

@push('js')
  <script>
    $(document).ready(function() {
      $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [5, 10, 25, -1],
          [5, 10, 25, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });
      var table = $('#datatable').DataTable();
      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });
      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });
      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
    });
  </script>
@endpush
