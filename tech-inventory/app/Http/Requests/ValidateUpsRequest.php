<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateUpsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'capacidad' => 'required',
            'tipo_ups' => 'required'
        ];
    }
}
