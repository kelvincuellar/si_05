<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateMonitores extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'tamaño' => 'required',
            'id_tipo_panel' => 'required',
            'resolucion' => 'required',
            'id_tipo_monitor' => 'required',
            'color' => 'required',
            'tiempo_respuesta' => 'required'
        ];
    }
}
