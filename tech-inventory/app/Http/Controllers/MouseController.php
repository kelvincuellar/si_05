<?php

namespace App\Http\Controllers;

use\App\models\mouse;
use\App\models\dispositivos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request\ValidateMouseRequest;

class MouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mouse = DB::table('mice')
        ->join('manos','mice.id_tipo_mano','=','manos.id')
        ->join('dispositivos', 'mice.id_dispositivo','=','disposiivos.id')
        ->select('mice.*','manos.mano','dispositivos.*')
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validados = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'botones' => 'required',
            'dpi' => 'required',
            'id_tipo_mano' => 'required',
            'sensor' => 'required'
        ]);



         $dispositivo = dispositivos::create([
            'marca' => $validados['marca'],
            'modelo' => $validados['modelo'],
            'serial' => $validados['serial'],
            'codigo' => $validados['codigo'],
            'id_pc' => (isset($validados['id_pc'])) ? $validados['id_pc']: null,
            'id_servidor' => (isset($validados['id_servidor'])) ? $validados['id_servidor']: null
         ]);

        mouse::create([
            'botones' => $validados['botones'],
            'dpi' => $validados['dpi'],
            'id_tipo_mano' => $validados['id_tipo_mano'],
            'sensor' => $validados['sensor'],
            'id_dispositivo' => $dispositivo->id
        ]);

        return back()->with('success', 'Dispositivo registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mouse = mouse::findOrFail($id);
        $mouse->botones = $request->botones;
        $mouse->dpi = $request->dpi;
        $mouse->id_tipo_mano = $request->id_tipo_mano;
        $mouse->sensor = $request->sensor;
        $mouse->id_dispositivo = $request->id_dispositivo;
        $mouse->save();

        return redirect()->route('inventario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        mouse::destroy($id);

        return back()->with('success', 'Registro eliminado correctamente!');
    }
}
