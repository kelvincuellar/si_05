<?php

namespace App\Http\Controllers;

use App\models\computadoras;
use App\models\dispositivos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request\CpuValidate;

class CpuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $computadores = DB::table('computadoras')
                        ->join('tipo_computadoras', 'computadoras.id_tipo_computadora','=','tipo_computadoras.id')
                        ->join('dispositivos', 'computadoras.id_dispositivo','=','disposiivos.id')
                        ->select('computadoras.*','tipo_computadoras.tipo_pc','dispositivos.*')
                        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validados = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'ram_pc' => 'required',
            'disco_pc' => 'required',
            'motherboard_pc' => 'required',
            'procesador_pc' => 'required',
            'id_tipo_computadora' => 'required'
        ]);



         $dispositivo = dispositivos::create([
            'marca' => $validados['marca'],
            'modelo' => $validados['modelo'],
            'serial' => $validados['serial'],
            'codigo' => $validados['codigo'],
            'id_pc' => (isset($validados['id_pc'])) ? $validados['id_pc']: null,
            'id_servidor' => (isset($validados['id_servidor'])) ? $validados['id_servidor']: null
         ]);

        computadoras::create([
            'ram_pc' => $validados['ram_pc'],
            'disco_pc' => $validados['disco_pc'],
            'motherboard_pc' => $validados['motherboard_pc'],
            'procesador_pc' => $validados['procesador_pc'],
            'id_tipo_computadora' => $validados['id_tipo_computadora'],
            'id_dispositivo' => $dispositivo->id
        ]);

        return back()->with('success', 'Dispositivo registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CpuValidate $request, $id)
    {
        $cpu = computadoras::findOrFail($id);
        $cpu->ram_pc = $request->ram_pc;
        $cpu->disco_pc = $request->disco_pc;
        $cpu->motherboard = $request->motherboard;
        $cpu->procesador_pc = $request->procesador_pc;
        $cpu->id_tipo_computadora = $request->id_tipo_computadora;
        $cpu->id_dispositivo = $request->id_dispositivo; $cpu->id_dispositivo = $request->id_dispositivo;
        $cpu->save();

        return redirect()->route('inventario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        computadoras::destroy($id);

        return back()->with('success', 'Registro eliminado correctamente!');
    }
}
