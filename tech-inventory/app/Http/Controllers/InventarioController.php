<?php

namespace App\Http\Controllers;

use App\models\ups;
use\App\models\monitores;
use App\models\tipo_panel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InventarioController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $monitores = DB::table('monitores')
        ->join('tipo_panel','monitores.id_tipo_panel','=','tipo_panel.id')
        ->join('tipo_monitor', 'monitores.id_tipo_monitor','=','tipo_monitor.id')
        ->join('dispositivos', 'monitores.id_dispositivo','=','dispositivos.id')
        ->select('monitores.*','tipo_panel.tipo_panel','tipo_monitor.tipo_monitor','dispositivos.marca','dispositivos.modelo', 'dispositivos.serial')
        ->get();

        $mouse = DB::table('mice')
        ->join('manos','mice.id_tipo_mano','=','manos.id')
        ->join('dispositivos', 'mice.id_dispositivo','=','dispositivos.id')
        ->select('mice.*','manos.mano','dispositivos.marca','dispositivos.modelo', 'dispositivos.serial')
        ->get();

        $Pc = DB::table('pcs')
        ->join('areas', 'pcs.id_area','=','areas.id')
        ->join('departamentos', 'areas.id_departamento','=','departamentos.id')
        ->select('pcs.*','areas.area','departamentos.departamento')
        ->get();

        $teclados = DB::table('teclados')
        ->join('tipo_teclados','teclados.id_tipo_teclado','=','tipo_teclados.id')
        ->join('dispositivos', 'teclados.id_dispositivo','=','dispositivos.id')
        ->select('teclados.*','tipo_teclados.tipo_teclado','dispositivos.marca','dispositivos.modelo', 'dispositivos.serial')
        ->get();

        $ups = DB::table('ups')
        ->join('dispositivos', 'ups.id_dispositivo','=','dispositivos.id')
        ->select('ups.*','dispositivos.marca','dispositivos.modelo', 'dispositivos.serial')
        ->get();

        $camaras = DB::table('camaras')
        ->join('dispositivos', 'camaras.id_dispositivo','=','dispositivos.id')
        ->select('camaras.*','dispositivos.marca','dispositivos.modelo', 'dispositivos.serial')
        ->get();

        $computadores = DB::table('computadoras')
                        ->join('tipos_computadoras', 'computadoras.id_tipo_computadora','=','tipos_computadoras.id')
                        ->join('dispositivos', 'computadoras.id_dispositivo','=','dispositivos.id')
                        ->select('computadoras.*','tipos_computadoras.tipo_pc','dispositivos.marca','dispositivos.modelo', 'dispositivos.serial')
                        ->get();

        $paneles = DB::table('tipo_panel')
                    ->select('id','tipo_panel')
                    ->get();

        $panel = array();

        foreach($paneles as $p){
            $panel[$p->id] = $p->tipo_panel;
        }

        $tipos = DB::table('tipo_monitor')
                    ->select('id','tipo_monitor')
                    ->get();

        $tipo = array();

        foreach($tipos as $t){
            $tipo[$t->id] = $t->tipo_monitor;
        }

        $type_pc = DB::table('tipos_computadoras')
        ->select('id','tipo_pc')
        ->get();

        $tipo_pc = array();

        foreach($type_pc as $p){
        $tipo_pc[$p->id] = $p->tipo_pc;
        }

        $manos = DB::table('manos')
        ->select('id','mano')
        ->get();

        $mano = array();

        foreach($manos as $p){
        $mano[$p->id] = $p->mano;
        }

        $tipo_teclados = DB::table('tipo_teclados')
        ->select('id','tipo_teclado')
        ->get();

        $tipo_teclado = array();

        foreach($tipo_teclados as $p){
        $tipo_teclado[$p->id] = $p->tipo_teclado;
        }




        return view('inventario.inventario', compact('monitores','mouse','Pc','teclados','ups','camaras','computadores', 'panel', 'tipo', 'tipo_pc', 'mano', 'tipo_teclado'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
