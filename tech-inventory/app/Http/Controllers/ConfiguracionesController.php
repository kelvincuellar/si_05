<?php

namespace App\Http\Controllers;

use App\models\areas;
use App\models\departamentos;
use App\models\perfiles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ConfiguracionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configuraciones.index-config');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewDepartamento()
    {
        $departamentos = departamentos::all();
        return view('configuraciones.departamento.departamento', compact('departamentos'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDepartamento(Request $request)
    {

      $validados =  $request->validate([
            'departamento' => 'required'
        ]);



        departamentos::create($validados);

      return  back()->with('success','Departamento creado correctamente!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDepartamento(Request $request, $id)
    {
        $validados =  $request->validate([
            'departamento' => 'required'
        ]);
        $departamento = departamentos::findOrFail($id);
        $departamento->departamento = $validados;
        $departamento->save();

        return redirect()->route('configuraciones.departamentos')->with('success','Departamento actualizado correctamente!');
    }

      /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDepartamento($id)
    {
        departamentos::destroy($id);
        return redirect()->route('configuraciones.departamentos')->with('success','Departamento eliminado correctamente!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewArea()
    {
        $areas =DB::table('areas')
        ->join('departamentos', 'areas.id_departamento', '=', 'departamentos.id')
        ->select('areas.*','departamentos.departamento')
        ->get();
        $departamentos = departamentos::all();
        $departamento = array();
        foreach($departamentos as $depa){
            $departamento[$depa->id] = $depa->departamento;
        }
        return view('configuraciones.area.areas', compact('areas','departamento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeArea(Request $request)
    {



      $validados =  $request->validate([
            'area' => 'required',
            'id_departamento' => 'required|numeric'
        ]);

       // dd($request->all());

      areas::create($validados);

        return redirect()->route('configuraciones.areas')->with('success','Área creada correctamente!');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateArea(Request $request, $id)
    {
        $validados =  $request->validate([
            'area' => 'required',
            'id_departmento' => 'required|numeric'
        ]);
        $area = areas::findOrFail($id);
        $area->save($validados);

        return redirect()->route('configuraciones.areas')->with('success','Área actualizada correctamente!');
    }

      /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyArea($id)
    {
        areas::destroy($id);
        return redirect()->route('configuraciones.areas')->with('success','Área eliminada correctamente!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPerfiles()
    {
        $perfiles = perfiles::all();
        return view('configuraciones.perfiles.perfiles', compact('perfiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePerfil(Request $request)
    {
      $validados =  $request->validate([
            'cargo' => 'required',
            'nombres' => 'required|string',
            'apellidos' => 'required|string'
        ]);

       $perfil = perfiles::create($validados);

       if($perfil != null){
        return redirect()->route('configuraciones.perfiles')->with('success','Perfil creado correctamente!');
    }else{
        return redirect()->route('configuraciones.perfiles')->with('error','El Perfil no pudo ser creado!');
    }

    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatePerfil(Request $request, $id)
    {
        $validados =  $request->validate([
            'cargo' => 'required',
            'nomobre' => 'required|string',
            'apellidos' => 'required|string'
        ]);
        $perfil = perfiles::findOrFail($id);
        $perfil->save($validados);
        return redirect()->route('configuraciones.perfiles')->with('success','Perfil actualizado correctamente!');
    }

      /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPerfil($id)
    {
        perfiles::destroy($id);
        return redirect()->route('configuraciones.perfiles')->with('success','Perfil eliminado correctamente!');
    }
}
