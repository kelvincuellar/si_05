<?php

namespace App\Http\Controllers;

use App\models\ups;
use App\models\dispositivos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request\ValidateUpsRequest;

class UpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ups = DB::table('ups')
        ->join('dispositivos', 'ups.id_dispositivo','=','disposiivos.id')
        ->select('ups.*','dispositivos.*')
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $validados = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'capacidad' => 'required',
            'tipo_ups' => 'required'
        ]);

         $dispositivo = dispositivos::create([
            'marca' => $validados['marca'],
            'modelo' => $validados['modelo'],
            'serial' => $validados['serial'],
            'codigo' => $validados['codigo'],
            'id_pc' => (isset($validados['id_pc'])) ? $validados['id_pc']: null,
            'id_servidor' => (isset($validados['id_servidor'])) ? $validados['id_servidor']: null
         ]);

        ups::create([
            'capacidad' => $validados['capacidad'],
            'tipo_ups' => $validados['tipo_ups'],
            'id_dispositivo' => $dispositivo->id
        ]);
        return back()->with('success', 'Dispositivo registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateUpsRequest $request, $id)
    {
        $ups = ups::findOrFail($id);
        $ups->capacidad = $request->capacidad;
        $ups->tipo_ups = $request->tipo_ups;
        $ups->id_dispositivo = $request->id_dispositivo;
        $ups->save();

        return redirect()->route('inventario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       ups::destroy($id);

       return back()->with('success', 'Registro eliminado correctamente!');
    }
}
