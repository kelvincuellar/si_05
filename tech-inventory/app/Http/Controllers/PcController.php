<?php

namespace App\Http\Controllers;

use App\models\pc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request\ValidatePcRequest;

class PcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Pc = DB::table('pcs')
        ->join('areas', 'pcs.id_area','=','areas.id')
        ->join('departamentos', 'areas.id_departamento','=','departamentos.id')
        ->select('pcs.*','areas.area','departamento.departamento')
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidatePcRequest $request)
    {
        pc::create($request->all());

        return back()->with('success', 'Dispositivo registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidatePcRequest $request, $id)
    {
        $pc = pc::findOrFail($id);
        $pc->modelo = $request->modelo;
        $pc->id_area = $request->id_area;
        $pc->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pc::destroy($id);

        return back()->with('success', 'Registro eliminado correctamente!');
    }
}
