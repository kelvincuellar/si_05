<?php

namespace App\Http\Controllers;

use App\models\teclados;
use App\models\dispositivos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request\ValidateTecladoRequest;

class TecladoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monitores = DB::table('teclados')
        ->join('tipo_teclados','teclados.id_tipo_teclado','=','tipo_teclados.id')
        ->join('dispositivos', 'teclados.id_dispositivo','=','disposiivos.id')
        ->select('teclados.*','tipo_teclados.tipo_teclad','dispositivos.*')
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validados = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'color' => 'required',
            'tamaño' => 'required',
            'idioma' => 'required',
            'id_tipo_teclado' => 'required'
        ]);

         $dispositivo = dispositivos::create([
            'marca' => $validados['marca'],
            'modelo' => $validados['modelo'],
            'serial' => $validados['serial'],
            'codigo' => $validados['codigo'],
            'id_pc' => (isset($validados['id_pc'])) ? $validados['id_pc']: null,
            'id_servidor' => (isset($validados['id_servidor'])) ? $validados['id_servidor']: null
         ]);

        teclados::create([
            'color' => $validados['color'],
            'idioma' => $validados['idioma'],
            'tamaño' => $validados['tamaño'],
            'id_tipo_teclado' => $validados['id_tipo_teclado'],
            'id_dispositivo' => $dispositivo->id
        ]);

        return back()->with('success', 'Dispositivo registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateTecladoRequest $request, $id)
    {
        $teclado = teclados::findOrFail($id);
        $teclado->color = $request->color;
        $teclado->tamaño = $request->tamaño;
        $teclado->idioma = $request->idioma;
        $teclado->id_tipo_teclado = $request->id_tipo_teclado;
        $teclado->id_dispositivo = $request->id_dispositivo;
        $teclado->save();

        return redirect()->route('inventario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        teclados::destroy($id);

        return back()->with('success', 'Registro eliminado correctamente!');
    }
}
