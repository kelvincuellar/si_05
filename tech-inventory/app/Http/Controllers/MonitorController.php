<?php

namespace App\Http\Controllers;

use App\models\dispositivos;
use\App\models\monitores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request\ValidateMonitores;

class MonitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monitores = DB::table('monitores')
                    ->join('tipo_panel','monitores.id_tipo_panel','=','tipo_panel.id')
                    ->join('tipo_monitor', 'monitores.id_tipo_monitor','=','tipo_monitor.id')
                    ->join('dispositivos', 'monitores.id_dispositivo','=','disposiivos.id')
                    ->select('monitores.*','tipo_panel.tipo_panel','tipo_monitor.tipo_monitor','dispositivos.*')
                    ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validados = $request->validate([
            'marca' => 'required',
            'modelo' => 'required',
            'serial' => 'required',
            'codigo' => 'required',
            'id_pc' => '',
            'id_servidor' => '',
            'tamaño' => 'required',
            'id_tipo_panel' => 'required',
            'resolucion' => 'required',
            'id_tipo_monitor' => 'required',
            'color' => 'required',
            'tiempo_respuesta' => 'required'
        ]);



         $dispositivo = dispositivos::create([
            'marca' => $validados['marca'],
            'modelo' => $validados['modelo'],
            'serial' => $validados['serial'],
            'codigo' => $validados['codigo'],
            'id_pc' => (isset($validados['id_pc'])) ? $validados['id_pc']: null,
            'id_servidor' => (isset($validados['id_servidor'])) ? $validados['id_servidor']: null
         ]);

        monitores::create([
            'tamaño' => $validados['tamaño'],
            'id_tipo_panel' => $validados['id_tipo_panel'],
            'resolucion' => $validados['resolucion'],
            'id_tipo_monitor' => $validados['id_tipo_monitor'],
            'color' => $validados['color'],
            'tiempo_respuesta' => $validados['tiempo_respuesta'],
            'id_dispositivo' => $dispositivo->id
        ]);

        return back()->with('success', 'Dispositivo registrado correctamente!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateMonitores $request, $id)
    {
        $monitor = monitores::findOrFail($id);
        $monitor->tamaño = $request->tamaño;
        $monitor->id_tipo_panel = $request->id_tipo_panel;
        $monitor->resolucion = $request->resolucion;
        $monitor->id_tipo_monitor = $request->id_tipo_monitor;
        $monitor->color = $request->color;
        $monitor->tiempo_respuesta = $request->tiempo_respuesta;
        $monitor->id_dispositivo = $request->id_dispositivo;
        $monitor->save();

        return redirect()->route('inventario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        monitores::destroy($id);

        return back()->with('success', 'Registro eliminado correctamente!');
    }
}
