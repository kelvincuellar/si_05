<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class perfiles extends Model
{
    protected $fillable = ['cargo','nombres','apellidos'];
}
