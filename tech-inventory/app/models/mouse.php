<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class mouse extends Model
{
    protected $table = 'mice';


    protected $fillable = ['botones','dpi','id_tipo_mano','sensor','id_dispositivo'];
}
