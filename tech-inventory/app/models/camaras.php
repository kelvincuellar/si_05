<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class camaras extends Model
{
    protected $table = 'camaras';

    protected $fillable = ['resolucion_grabacion','microfono','id_dispositivo'];
}
