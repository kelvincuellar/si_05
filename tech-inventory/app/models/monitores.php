<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class monitores extends Model
{
    protected $table = 'monitores';


    protected $fillable = ['tamaño','id_tipo_panel','resolucion','id_tipo_monitor','color','tiempo_respuesta','id_dispositivo'];
}
