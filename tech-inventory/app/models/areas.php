<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class areas extends Model
{
    protected $table = 'areas';

    protected $fillable = ['area','id_departamento'];
}
