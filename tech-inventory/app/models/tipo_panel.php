<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class tipo_panel extends Model
{
    protected $table = 'tipo_panel';

    protected $fillable = ['tipo_monitor'];
}
