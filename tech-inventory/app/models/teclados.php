<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class teclados extends Model
{
    protected $table = 'teclados';


    protected $fillable = ['color','tamaño','idioma','id_tipo_teclado','id_dispositivo'];
}
