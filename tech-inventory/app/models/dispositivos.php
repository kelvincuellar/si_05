<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class dispositivos extends Model
{
    protected $table = 'dispositivos';


    protected $fillable = ['marca', 'modelo','serial','codigo','id_pc','id_servidor'];
}
