<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class ups extends Model
{
    protected $table = 'ups';


    protected $fillable = ['capacidad','tipo_ups','id_dispositivo'];
}
