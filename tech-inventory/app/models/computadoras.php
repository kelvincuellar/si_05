<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class computadoras extends Model
{
    protected $table = 'computadoras';


    protected $fillable = ['ram_pc','disco_pc','motherboard_pc','procesador_pc','id_tipo_computadora','id_dispositivo'];
}
