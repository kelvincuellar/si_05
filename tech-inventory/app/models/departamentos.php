<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class departamentos extends Model
{
    protected $fillable = ['departamento'];
}
