<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
Route::get('/home', 'InventarioController@index')->name('home');
Route::get('/', 'InventarioController@index')->name('dasboard');
Route::get('/inventario', 'InventarioController@create')->name('inventario');
Route::get('/configuraciones', 'ConfiguracionesController@index');
Route::get('/configuraciones/departamento', 'ConfiguracionesController@viewDepartamento')->name('configuraciones.departamentos');
Route::get('/configuraciones/area','ConfiguracionesController@viewArea')->name('configuraciones.areas');
Route::get('/configuraciones/perfil', 'ConfiguracionesController@viewPerfiles')->name('configuraciones.perfiles');


//post routes
Route::post('/configuraciones/crear-departamento', 'ConfiguracionesController@storeDepartamento')->name('store.departamento');
Route::post('/configuraciones/crear-area', 'ConfiguracionesController@storeArea')->name('store.area');
Route::post('/configuraciones/crear-perfil', 'ConfiguracionesController@storePerfil')->name('store.perfil');

Route::post('/camara/store', 'CamaraController@store')->name('store.camara');
Route::post('/teclado/store', 'TecladoController@store')->name('store.teclado');
Route::post('/mouse/store', 'MouseController@store')->name('store.mouse');
Route::post('/cpu/store', 'CpuController@store')->name('store.cpu');
Route::post('/pc/store', 'PcController@store')->name('store.pc');
Route::post('/ups/store', 'UpsController@store')->name('store.ups');
Route::post('/monitor/store', 'MonitorController@store')->name('store.monitor');

Route::get('/camara/edit/{id}', 'CamaraController@edit')->name('edit.camara');
Route::get('/teclado/edit/{id}', 'TecladoController@edit')->name('edit.teclado');
Route::get('/mouse/edit/{id}', 'MouseController@edit')->name('edit.mouse');
Route::get('/cpu/edit/{id}', 'CpuController@edit')->name('edit.cpu');
Route::get('/pc/edit/{id}', 'PcController@edit')->name('edit.pc');
Route::get('/ups/edit/{id}', 'UpsController@edit')->name('edit.ups');
Route::get('/monitor/edit/{id}', 'MonitorController@edit')->name('edit.monitor');

//put routes
Route::put('/configuraciones/update-departamento/{id}', 'ConfiguracionesController@updateDepartamento')->name('update.departamento');
Route::put('/configuraciones/update-area/{id}', 'ConfiguracionesController@updateArea')->name('update.area');
Route::put('/configuraciones/update-perfil/{id}', 'ConfiguracionesController@updatePerfil')->name('update.perfil');

Route::put('/camara/update/{id}', 'CamaraController@update')->name('update.camara');
Route::put('/teclado/update/{id}', 'TecladoController@update')->name('update.teclado');
Route::put('/mouse/update/{id}', 'MouseController@update')->name('update.mouse');
Route::put('/cpu/update/{id}', 'CpuController@update')->name('update.cpu');
Route::put('/pc/update/{id}', 'PcController@update')->name('update.pc');
Route::put('/ups/update/{id}', 'UpsController@update')->name('update.ups');
Route::put('/monitor/update/{id}', 'MonitorController@update')->name('update.monitor');


//delete routes
Route::delete('/configuraciones/delete-departamento/{id}', 'ConfiguracionesController@destroyDepartamento')->name('delete.departamento');
Route::delete('/configuraciones/delete-area/{id}', 'ConfiguracionesController@destroyArea')->name('delete.area');
Route::delete('/configuraciones/delete-perfil/{id}', 'ConfiguracionesController@destroyPerfil')->name('delete.perfil');

Route::delete('/camara/delete/{id}', 'CamaraController@destroy')->name('delete.camara');
Route::delete('/teclado/delete/{id}', 'TecladoController@destroy')->name('delete.teclado');
Route::delete('/mouse/delete/{id}', 'MouseController@destroy')->name('delete.mouse');
Route::delete('/cpu/delete/{id}', 'CpuController@destroy')->name('delete.cpu');
Route::delete('/pc/delete/{id}', 'PcController@destroy')->name('delete.pc');
Route::delete('/ups/delete/{id}', 'UpsController@destroy')->name('delete.ups');
Route::delete('/monitor/delete/{id}', 'MonitorController@destroy')->name('delete.monitor');


});
