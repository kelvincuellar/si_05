<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositivos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('marca');
            $table->string('modelo');
            $table->string('serial');
            $table->string('codigo');
            $table->integer('id_pc')->nullable()->unsigned();
            $table->foreign('id_pc')->references('id')->on('pcs');
            $table->integer('id_servidor')->nullable()->unsigned();
            $table->foreign('id_servidor')->references('id')->on('servidores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositivos');
    }
}
