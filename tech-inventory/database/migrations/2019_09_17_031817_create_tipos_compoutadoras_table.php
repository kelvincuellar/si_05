<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposCompoutadorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_computadoras', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tipo_pc');
            $table->timestamps();
        });

        DB::table('tipos_computadoras')->insert(
            array(
                'tipo_pc' => 'Laptop',
                'tipo_pc' => 'All in one',
                'tipo_pc' => 'Desktop'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_compoutadoras');
    }
}
