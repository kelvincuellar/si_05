<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoPanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_panel', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tipo_panel');
            $table->timestamps();
        });

        DB::table('tipo_panel')->insert(
            array(
                'tipo_panel' => 'TN',
                'tipo_panel' => 'IPS'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_panels');
    }
}
