<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('mano');
            $table->timestamps();
        });

        DB::table('manos')->insert(
            array(
                'mano' => 'Derecha',
                'mano' => 'Izquierda'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manos');
    }
}
