<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComputadorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computadoras', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('ram_pc');
            $table->string('disco_pc');
            $table->string('motherboard_pc');
            $table->string('procesador_pc');
            $table->integer('id_tipo_computadora')->unsigned();
            $table->foreign('id_tipo_computadora')->references('id')->on('tipos_computadoras');
            $table->integer('id_dispositivo')->unsigned();
            $table->foreign('id_dispositivo')->references('id')->on('dispositivos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computadoras');
    }
}
