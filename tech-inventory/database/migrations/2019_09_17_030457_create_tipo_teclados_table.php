<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoTecladosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_teclados', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tipo_teclado');
            $table->timestamps();
        });

        DB::table('tipo_teclados')->insert(
            array(
                'tipo_teclado' => 'Membrana',
                'tipo_teclado' => 'Mécanico'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_teclados');
    }
}
