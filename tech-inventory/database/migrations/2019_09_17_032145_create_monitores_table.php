<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitores', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('tamaño');
            $table->integer('id_tipo_panel')->unsigned();
            $table->foreign('id_tipo_panel')->references('id')->on('tipo_panel');
            $table->string('resolucion');
            $table->integer('id_tipo_monitor')->unsigned();
            $table->foreign('id_tipo_monitor')->references('id')->on('tipo_monitor');
            $table->string('color');
            $table->string('tiempo_respuesta');
            $table->integer('id_dispositivo')->unsigned();
            $table->foreign('id_dispositivo')->references('id')->on('dispositivos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitores');
    }
}
