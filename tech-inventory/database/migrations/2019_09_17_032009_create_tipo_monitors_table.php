<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_monitor', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tipo_monitor');
            $table->timestamps();
        });

        DB::table('tipo_monitor')->insert(
            array(
                'tipo_monitor' => 'Curvo',
                'tipo_monitor' => 'Gaming',
                'tipo_monitor' => 'Profesional'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_monitors');
    }
}
